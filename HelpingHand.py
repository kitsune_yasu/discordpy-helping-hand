# Licensed under the EUPL, Version 1.2
# You may not use this work except in compliance with the Licence.
# You may obtain a copy of the Licence at:
#
# https://joinup.ec.europa.eu/software/page/eupl
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the Licence is distributed on an "AS IS" basis,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the Licence for the specific language governing permissions and
# limitations under the Licence.

import itertools
import logging

import discord
from discord.ext.commands import HelpCommand, Group


class HelpingHand(HelpCommand):
    """Advanced Help-Command based on the Default one but with Embeds.

        This inherits from :class:`HelpCommand`.

        It extends it with the following attributes.

        Attributes
        ------------
        delete_after: :class:`int`
            Deletes the Help-Command after the given time. -1 will disable this feature (Default: 10)
        force_dm: :class:`bool`
            If True this will always send Help-Messages via DM.
            If False it will send the Help-Messages into the current Channel (Default)
        help_time: :class:`int`
            The time in seconds help messages will be shown. Default is 120.
        help_color: :class:`discord.Color`
            The color to use for the help embeds. Default is teal.
        error_time: :class:`int`
            The time in seconds error messages will be shown. Default is 15.
        error_color: :class:`discord.Color`
            The color to use for the error embeds. Default is dark_teal.
        sort_name_for_uncategorized_commands: :class:`str`
            This will decide where Uncategorized Commands will be sorted.
            Defaults to 'ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ' to sort it at the end.
            (If you use Emotes in your Cog-Name you need to change this to something that is sorted behind your used Emotes)
        localize: :class:`typing.Union[typing.Awaitable, typing.Callable, dict]`
            Allows you to translate strings used in this Help-Command. Can be one of the following:
            - a Dictionary with the original text as Key and the Translation as value
            - a Callable which takes two arguments (context, original_text)
            - an Awaitable which takes two arguments (context, original_text)
        log_missing_translations: :class:`bool`
            If localize is a dict and is missing a key this decides if the key will be logged to the logger. (Default: True)
        sort_cogs: :class:`bool`
            Sort Cogs by qualified name (Default: True)
        sort_commands: :class:`bool`
            Sort Commands by qualified name (Default: True)
        sort_subcommands: :class:`bool`
            Sort Commands within Groups by qualified name (Default: False)
        """

    def __init__(self, **options):
        self.delete_after = options.pop('delete_after', 10)
        self.force_dm = options.pop('force_dm', False)
        self.help_time = options.pop('help_time', 120)
        self.help_color = options.pop('help_color', discord.Color.teal())
        self.error_time = options.pop('error_time', 15)
        self.error_color = options.pop('error_color', discord.Color.dark_teal())
        self.sort_name_for_uncategorized_commands = options.pop('sort_name_for_uncategorized_commands', 'ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ')
        self.localize = options.pop('localize', None)
        self.log_missing_translations = options.pop('log_missing_translations', True)
        self.sort_cogs = options.pop('sort_cogs', True)
        self.sort_commands = options.pop('sort_commands', True)
        self.sort_subcommands = options.pop('sort_subcommands', False)

        super(HelpingHand, self).__init__(**options)

    async def _translate(self, base: str) -> str:
        if self.localize is None:
            return base
        elif isinstance(self.localize, dict):
            if base in self.localize:
                return self.localize[base]
            else:
                if self.log_missing_translations:
                    logging.getLogger(__name__).warning('Missing Translation for "{0}"'.format(base))
                return base
        elif callable(self.localize):
            return await discord.utils.maybe_coroutine(self.localize, self.context, base)
        else:
            return base

    async def _send(self, embed):
        if self.delete_after > -1:
            await self.context.message.delete(delay=self.delete_after)

        await self.get_destination().send(
            embed=embed,
            delete_after=self.help_time
        )

    def _create_command_fields(self, embed, commands):
        for command in commands:
            if not command.full_parent_name == "":
                parent = command.full_parent_name + " "
            else:
                parent = ""

            if command.usage is not None:
                embed.add_field(
                    name=command.qualified_name,
                    value="{2.description}\n```{0}{1}{2.name} {2.usage}```".format(self.context.prefix, parent, command),
                    inline=False
                )
            else:
                embed.add_field(
                    name=command.qualified_name,
                    value="{2.description}\n```{0}{1}{2.name}```".format(self.context.prefix, parent, command),
                    inline=False
                )

    def get_destination(self):
        if self.force_dm:
            return self.context.author
        else:
            return self.context.channel

    async def command_not_found(self, string):
        return (await self._translate('Could not find a command or alias named `{0}`')).format(string)

    async def subcommand_not_found(self, command, string):
        if isinstance(command, Group) and len(command.all_commands) > 0:
            return (await self._translate('`{0.qualified_name}` has no subcommand named `{1}`!')).format(command, string)
        else:
            return (await self._translate('`{0.qualified_name}` has no subcommands!')).format(command)

    async def send_error_message(self, error):
        destination = self.get_destination()

        if self.delete_after > -1:
            await self.context.message.delete(delay=self.delete_after)

        await destination.send(
            embed=discord.Embed(
                title=error,
                description=(await self._translate('Use `{0}{1}` to see the full help page.')).format(self.context.prefix, self.invoked_with),
                color=self.error_color
            ).set_footer(
                icon_url=self.context.author.avatar_url,
                text=(await self._translate('Created for {0}')).format(self.context.author.display_name)
            ),
            delete_after=self.error_time
        )

    async def send_bot_help(self, mapping):
        uc_name = self.sort_name_for_uncategorized_commands

        def get_category_name(command, *, uncategorized_commands=uc_name):
            cog = command.cog
            return cog.qualified_name if cog is not None else uncategorized_commands

        def get_category(command):
            cog = command.cog
            return cog

        filtered = await self.filter_commands(self.context.bot.commands, sort=self.sort_cogs, key=get_category_name)
        to_iterate = itertools.groupby(filtered, key=get_category)

        modules = discord.Embed(
            title=await self._translate('Overview'),
            description=await self._translate('List of available modules'),
            color=self.help_color
        ).set_footer(
            icon_url=self.context.author.avatar_url,
            text=(await self._translate('Created for {0}')).format(self.context.author.display_name)
        )

        for category, commands in to_iterate:
            if category is not None:
                modules.add_field(
                    name=category.qualified_name,
                    value="{0}\n`{1}`".format(category.description, '`, `'.join(command.qualified_name for command in commands)),
                    inline=False
                )
            else:
                modules.add_field(
                    name=await self._translate('No Category'),
                    value="`{0}`".format('`, `'.join(command.qualified_name for command in commands)),
                    inline=False
                )

        await self._send(modules)

    async def send_cog_help(self, cog):
        commands = await self.filter_commands(cog.get_commands(), sort=self.sort_commands)

        modules = discord.Embed(
            title=(await self._translate('Commands in Module `{0}`')).format(cog.qualified_name),
            description=cog.description,
            color=self.help_color
        ).set_footer(
            icon_url=self.context.author.avatar_url,
            text=(await self._translate('Created for {0}')).format(self.context.author.display_name)
        )

        self._create_command_fields(modules, commands)
        await self._send(modules)

    async def send_group_help(self, group):
        if not group.full_parent_name == "":
            parent = group.full_parent_name + " "
        else:
            parent = ""

        commands = await self.filter_commands(group.commands, sort=self.sort_subcommands)

        if group.usage is not None:
            modules = discord.Embed(
                title=(await self._translate('Help for Command `{0}`')).format(group.qualified_name),
                description="{2.description}\n```{0}{1}{2.name} {2.usage}```".format(self.context.prefix, parent, group),
                color=self.help_color
            )
        else:
            modules = discord.Embed(
                title=(await self._translate('Help for Command `{0}`')).format(group.qualified_name),
                description="{0.description}".format(group),
                color=self.help_color
            )

        modules.set_footer(
            icon_url=self.context.author.avatar_url,
            text=(await self._translate('Created for {0}')).format(self.context.author.display_name)
        )

        self._create_command_fields(modules, commands)
        await self._send(modules)

    async def send_command_help(self, command):
        if not command.full_parent_name == "":
            parent = command.full_parent_name + " "
        else:
            parent = ""

        if command.usage is not None:
            modules = discord.Embed(
                title=(await self._translate('Help for Command `{0}`')).format(command.qualified_name),
                description="{2.description}\n```{0}{1}{2.name} {2.usage}```".format(self.context.prefix, parent, command),
                color=self.help_color
            )
        else:
            modules = discord.Embed(
                title=(await self._translate('Help for Command `{0}`')).format(command.qualified_name),
                description="{0.description}".format(command),
                color=self.help_color
            )

        modules.set_footer(
            icon_url=self.context.author.avatar_url,
            text=(await self._translate('Created for {0}')).format(self.context.author.display_name)
        )

        await self._send(modules)
