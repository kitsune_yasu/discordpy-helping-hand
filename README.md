# [discord.py](https://github.com/Rapptz/discord.py) - Helping Hand

Helping Hand is a small Tool which provides a more beautiful view for the default Help-Command that comes with [discord.py](https://github.com/Rapptz/discord.py)

## Usage
1. Place the `HelpingHand.py` within your projects directory
2. Set the `help_command` Parameter when creating a new Bot-Instance:
```py
bot = discord.Bot(
	command_prefix='#',
	help_command=HelpingHand()
)

# Or:

class YourBot(discord.ext.commands.AutoShardedBot):
    def __init__(self):
        super().__init__(
            command_prefix='#',
            help_command=HelpingHand()
        )
		# ...
```
3. Use [discord.py annotations](https://discordpy.readthedocs.io/en/latest/ext/commands/api.html?highlight=description#command) to document your commands:
```py
@commands.command(
	name="someCommand",
	description="This does something, but nobody knows what...",
	usage="<SomeMember> <SomeText>",
	aliases=["sc", "something"],
	hidden=False
)
async def someCommand(ctx, member: discord.Member, *, text):
	# ...
```
4. You can also document your Cogs if you use them:
```py
class SomeCog(commands.Cog, name="Some Module with Commands"):
    """
    Some more descriptive Text about this Cog...
    """
```
5. Run the Bot and test the command
6. Watch this repo in case of updates (optional, but recommended)
7. Place some credits for the author (optional, but highly appreciated)

## Configuration
Additionally to the default parameteres the Help-Command receives you can add the following parameters for further customization.

**delete_after**  (Default: 10)
> Delete the message that triggered the command after this time in seconds.
> You can use `-1` if you want to keep those commands.

**force_dm** (Default: False)
> If `True` this will always send Help-Messages via DM.
> If `False` it will send the Help-Messages into the current Channel

**help_time** (Default: 120)
> Deletes the response of the bot after set time in seconds.

**help_color** (Default: discord.Color.teal())
> The color to use for the help embeds.

**error_time** (Default: 15)
> Deletes the response in case of an error after set time in seconds.

**error_color** (Default: discord.Color.dark_teal())
> The color to use for the error embeds.

**sort_name_for_uncategorized_commands** (Default: 'ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ')
> This will decide where Uncategorized Commands will be sorted.
> The Default is set to sort them at the end.
> If you use Emotes in your Cog-Name you need to change this to something that is sorted behind your used Emotes.

**localize** (Default: None)
> Allows you to translate strings used in this Help-Command. Can be one of the following:
> - a Dictionary with the original text as Key and the Translation as value
> - a Callable which takes two arguments (context, original_text)
> - an Awaitable which takes two arguments (context, original_text)
```py
# Example for German Translations
translations = {
		"Could not find a command or alias named `{0}`": "Der Befehl/Alias `{0}` konnte nicht gefunden werden.",
		"`{0.qualified_name}` has no subcommand named `{1}`!": "`{0.qualified_name}` hat keinen Subcommand mit dem Namen `{1}`!",
		"`{0.qualified_name}` has no subcommands!": "`{0.qualified_name}` hat keine Subcommands!",
		"Use `{0}{1}` to see the full help page.": "Nutze `{0}{1}` um die vollständige Hilfe einzusehen.",
		"Created for {0}": "Erzeugt für {0}",
		"Overview": "Überblick",
		"List of available modules": "Liste aller verfügbaren Module",
		"No Category": "Keine Kategorie",
		"Commands in Module `{0}`": "Befehle in Modul `{0}`",
		"Help for Command `{0}{1}`": "Hilfe für den `{0}{1}`-Befehl",
	}

HelpingHand(
	localize=translations
)
```

**log_missing_translations** (Default: True)
> If localize is a dict and is missing a key this decides if the key will be logged to the logger.

**sort_cogs** (Default: True)
> Decide if Cogs should be sorted by their qualified name.

**sort_commands** (Default: True)
> Decide if Commands should be sorted by their qualified name.

**sort_subcommands** (Default: False)
> Decide if subcommands of a group should be sorted by their qualified name.

## License
This project is licensed under the EUPL v1.2
An overview of the permissions, conditions and limitation can be found [here](https://choosealicense.com/licenses/eupl-1.2/). You can find the license in other languages [here](https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12).

---

*This project is not associated with the Developers of [discord.py](https://github.com/Rapptz/discord.py)*
